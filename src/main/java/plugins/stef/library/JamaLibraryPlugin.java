package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Jama Matrix Package library for Icy
 * 
 * @author Stephane Dallongeville
 */
public class JamaLibraryPlugin extends Plugin implements PluginLibrary
{
    //
}
